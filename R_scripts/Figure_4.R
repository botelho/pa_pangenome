# load libraries

library(ggplot2)
library(ggpubr)
library(scales)
library(cowplot)

# load file Table S2

Table_S2 <- read.delim("~/path_to_file/Table_S2.txt")

# calculate means for phylogroups

compare_means(Genome.size ~ CRISPR.Cas, data = Table_S2, group.by = "Phylogroup")
my_comparisons_4 <- list( c("Yes", "No"))


# create boxplot

CRISPR_per_phylo <- ggboxplot(Table_S2, x = "CRISPR.Cas", y = "Genome.size", 
                                   color = "Phylogroup", palette = "jco", add = "jitter") +
  stat_compare_means(comparisons = my_comparisons_4, label = "p.signif", size = 3) +
  stat_compare_means(label.y =8000000, size = 4) +
  theme(axis.text.x= element_text(size = 12)) +
  ylab("Genome size (Mbp)") +
  xlab("CRISPR-Cas") +
  labs(fill = "Phylogroup") +
  scale_y_continuous(labels = function(x) format(x/1000000)) +
  facet_grid(~Phylogroup)

# plot

CRISPR_per_phylo

# calculate means for phylogroups (masked)

compare_means(Masked.genome.size ~ CRISPR.Cas, data = Table_S2, group.by = "Phylogroup")
my_comparisons_5 <- list( c("Yes", "No"))


# create boxplot

masked_CRISPR_per_phylo <- ggboxplot(Table_S2, x = "CRISPR.Cas", y = "Masked.genome.size", 
                              color = "Phylogroup", palette = "jco", add = "jitter") +
  stat_compare_means(comparisons = my_comparisons_5, label = "p.signif", size = 3) +
  stat_compare_means(label.y =7000000, size = 4) +
  theme(axis.text.x= element_text(size = 12)) +
  ylab("Masked genome size (Mbp)") +
  xlab("CRISPR-Cas") +
  labs(fill = "Phylogroup") +
  scale_y_continuous(labels = function(x) format(x/1000000)) +
  facet_grid(~Phylogroup)

# combine plot

Fig4 <- plot_grid(CRISPR_per_phylo + theme(axis.title.x=element_blank(), axis.text.x=element_blank(),  legend.position="top"),
                    masked_CRISPR_per_phylo + theme(legend.position = "none"), 
                    labels = "AUTO", ncol= 1)