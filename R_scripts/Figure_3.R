# load libraries

library(ggplot2)
library(ggpubr)
library(scales)
library(cowplot)

# load Table S2

Table_S2 <- read.delim("~/path_to_file/Table_S2.txt")

# calculate means for genome size per phylogroup

compare_means(Genome.size ~ Phylogroup, data = Table_S2)
my_comparisons <- list( c("A", "B"), c("A", "C"), c("B", "C"))

# create boxplot

Genome_size_per_phylo <- ggboxplot(Table_S2, x = "Phylogroup", y = "Genome.size", 
                                   color = "Phylogroup", palette = "jco", add = "jitter") +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", size = 4) +
  stat_compare_means(label.y =8700000, size = 5) +
  theme(axis.text.x= element_text(size = 12)) +
  ylab("Genome size (Mb)") +
  labs(fill = "Phylogroup") +
  scale_y_continuous(labels = function(x) format(x/1000000))

# calculate means for masked genome size per phylogroup

compare_means(Masked.genome.size ~ Phylogroup, data = Table_S2)
my_comparisons_1 <- list( c("A", "B"), c("A", "C"), c("B", "C"))

# create boxplot

Masked_size_per_phylo <- ggboxplot(Table_S2, x = "Phylogroup", y = "Masked.genome.size", 
                                   color = "Phylogroup", palette = "jco", add = "jitter") +
  stat_compare_means(comparisons = my_comparisons_1, label = "p.signif", size = 4) +
  stat_compare_means(label.y =7300000, size = 5) +
  theme(axis.text.x= element_text(size = 12)) +
  ylab("Masked genome size (Mbp)") +
  labs(fill = "Phylogroup") +
  scale_y_continuous(labels = function(x) format(x/1000000))

legend_b <- get_legend(Masked_size_per_phylo + theme(legend.position="bottom"))

# combine plots

p3 <- plot_grid(Genome_size_per_phylo + theme(legend.position="none"),
                Masked_size_per_phylo + theme(legend.position = "none"), 
                labels = "AUTO")

Fig3 <- plot_grid(p3, legend_b, ncol = 1, rel_heights = c(1, .2))
