# PA_pangenome

This is the code repository for reproducing the analyses in the paper:

[Phylogroup-specific variation shapes the clustering of antimicrobial resistance genes and defence systems across regions of genome plasticity](https://www.biorxiv.org/content/10.1101/2022.04.24.489302v2)

To trim the Illumina reads, we used [Trim Galore](https://github.com/FelixKrueger/TrimGalore) v0.6.6 with the following command:

`'for file1 in *_R1.fastq.gz; do file2=${file1/_R1/_R2}; trim_galore --paired --quality 10 $file1 $file2; done`

To build hybrid assemblies (PacBio reads + trimmed Illumina reads), we used [Unicycler](https://github.com/rrwick/Unicycler) v0.4.8 with the following command:

`unicycler -1 trimmed_R1.fq.gz -2 trimmed_R2.fq.gz -l Pacbio.fastq -o strain_name -t 32`

When necessary, we used the option `--mode bold` to improve the assembly contiguity (please check the Methods section in the manuscript for more details)

To download the _P. aeruginosa_ assemblies from NCBI's RefSeq database, we used [PanACoTA](https://github.com/gem-pasteur/PanACoTA) v1.2.0 with the following parameters:

`PanACoTA prepare -o refseq --nbcont 100 -T 287 -g "Pseudomonas aeruginosa" -s refseq --max_dist 0.05 -p 16`

With the `--max_dist` argument, genomes whose distance to the reference is not between 1e-4 and 0.05 are discarded. This step removes very distantly related genomes and also redundant genomes. The `-T` argument refers to species taxid. With `--nbcont`, assemblies with more than 100 contigs are discarded.

Next, we used:

`PanACoTA annotate --info LSTINFO-Pseudomonas_aeruginosa-filtered-0.0001_0.05_Hannover_and_refseq.txt -r annotated -n PSAE --threads 32`

To calculate the average nucleotide identity (ANI) between the 2009 genomes, we used [fastANI v1.33](https://github.com/ParBLiSS/FastANI) with the following parameters:

`fastANI --ql 2009.txt --rl 2009.txt -t 32 -o 2009_fastani.txt`

We then used the renamed replicons as input to build a pangenome for the whole collection in [PPanGGOLiN](https://github.com/labgem/PPanGGOLiN) v1.1.136, using the `panrgp` subcommand with the following parameters:

`ppanggolin panrgp --fasta names_locations.txt -o panrgp_2009 -c 32 --rarefaction`

To build a softcore-genome alignment, we used the `msa` subcommand from ppanggolin with the following parameters:

`ppanggolin msa -p pangenome.h5 -o msa_softcore_DNA -c 32 --partition softcore --phylo --source dna`

To infer a phylogenomic tree from the softcore-genome alignment, we used the General Time Reversible (GTR) model of nucleotide substitution in [IQ-TREE](http://www.iqtree.org/) v2.1.2 with the following parameters:

`iqtree -s softcore_0.95_genome_alignment.aln -T 32 --mem 170G -m GTR`

To correct for recombination events, we used [ClonalFrameML](https://github.com/xavierdidelot/ClonalFrameML) v1.12 with the following command:

`ClonalFrameML IQtree2/PSAE2009.grp.aln.iqtree_tree.treefile Phylo-PSAE2009/PSAE2009.grp.aln clonalframeml_PSAE2009`

Both trees were plotted in [iTOL](https://itol.embl.de/) v6. We then ran separate pangenomes for genomes belonging to the same phylogroup, using the `panrgp` subcommand with the same parameters as abovementioned.

To extract all relevant information about each pangenome (whole collection and for each phylogroup), we used the `info` subcommand from ppanggolin:

`ppanggolin info -p pangenome.h5 --content`

To classify core and accessory genes across genomes from different phylogroups, we used the following [R script](https://github.com/ghoresh11/twilight). We used the gene presence/absence output from the whole collection and the grouping of our genomes according to the phylogroup:

`Rscript classify_genes.R -p gene_presence_absence.Rtab -g grouping.txt`

To mask genomes, we used the RGPs coordinates determined by panrgp for each phylogroup as input in [bedtools maskfasta](https://bedtools.readthedocs.io/en/latest/content/tools/maskfasta.html) v2.30.0 with the following command:

`bedtools maskfasta -fi all_genomes.fasta -bed RGPs.txt -fo masked_genomes.fasta`

To extract the nucleotide sequence of the RGPs, we used [bedtools getfasta](https://bedtools.readthedocs.io/en/latest/content/tools/getfasta.html) v2.30.0 with the following command:

`bedtools getfasta -fi all_genomes.fasta -bed RGPs.txt -fo RGPs.fasta`

To calculate the Jaccard Index between the RGPs, we used [BinDash](https://github.com/zhaoxiaofei/bindash) v0.2.1 commit 78e0d46-clean. We first used the `sketch` subcommand to reduce multiple sequences into one sketch, followed by the `dist` subcommand, to estimate distance (and relevant statistics) between RGPs in query sketch and RGPs in target-sketch. We used the following command:

`bindash sketch --outfname=all_RGPs.sketch *.fasta && bindash dist all_RGPs.sketch --nthreads=16 > bindash_results.tsv`

To explore the functional categories of the proteins encoded on RGPs and masked genomes from different phylogroups, we clustered each group of proteins with [MMseqs2](https://github.com/soedinglab/MMseqs2) v13.45111, with the following command (the below example was used to cluster the proteins from masked genomes in phylogroup C):

`mmseqs createdb all.faa masked_3 && mmseqs linclust --min-seq-id 0.8 masked_3 masked_3_clu tmp && mmseqs createsubdb masked_3_clu masked_3 masked_3_clu_rep && mmseqs convert2fasta masked_3_clu_rep masked_3_clu_rep.fasta`

Each group of clustered proteins was then used as input for [EggNOG-mapper](http://eggnog-mapper.embl.de/) v2. To calculate the relative number of COG categories for each group of clustered proteins, we used the command:

`awk -F "\t" '{print $7}' out.emapper.annotations > COGs.txt  && grep -o '[A-Z]' COGs.txt | sort | uniq -c > uniq_counts_COGs.txt`

To look for CRISPR-Cas systems, we used [CRISPRCasTyper](https://github.com/Russel88/CRISPRCasTyper) v1.2.3 with the following command:

`for F in *.fasta ; do N=$(basename $F .fasta)_cctyper ; cctyper $F $N ; done`

To look for AMR genes, we used [AMRFinder](https://github.com/ncbi/amr/wiki/AMRFinder-database) v3.10.18 with the following command:

`for F in *.fasta ; do amrfinder -n $F -O Pseudomonas_aeruginosa --nucleotide_output amrfinder_detected_nts.fna --threads 8 > amrfinder_$F.tab; done`

To look for virulence genes, we used the integrated [VFDB](http://www.mgc.ac.cn/VFs/) in [abricate](https://github.com/tseemann/abricate) v1.0.1 with the command:

`for F in *.fasta ; do abricate --db vfdb --threads 8 $F > vfdb_$F.tab ; done && abricate --summary vfdb_*.tab > vfdb.txt`

To look for defence systems, we first annotated our fasta files with [Prokka](https://github.com/tseemann/prokka) v1.4.6 with the following command:

`for F in *.fasta; do N=$(basename $F .fasta)_prokka ; prokka --locustag $N --outdir $N --prefix $N  $F --cpus 32 ; done`

We then used the protein files created by prokka as input in [defense-finder](https://github.com/mdmparis/defense-finder) v0.0.11 with the command:

`for F in *.faa ; do N=$(basename $F .faa)_defensefinder ; defense-finder run -o $N -w 8 $F ; done`

To look for ICEs/IMEs on complete genomes, we used the genbank files created by prokka as input in the standalone-version of [ICEfinder](https://bioinfo-mml.sjtu.edu.cn/ICEfinder/index.php) with the following command:

`perl ICEfinder_local.pl file.txt`

