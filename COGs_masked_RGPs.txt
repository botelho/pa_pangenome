Region	Phylogroup	COG category	Relative Count	Total count
Masked genomes	A	A	9	31734
Masked genomes	A	B	10	31734
Masked genomes	A	C	1954	31734
Masked genomes	A	D	404	31734
Masked genomes	A	E	2904	31734
Masked genomes	A	F	470	31734
Masked genomes	A	G	1514	31734
Masked genomes	A	H	812	31734
Masked genomes	A	I	1261	31734
Masked genomes	A	J	718	31734
Masked genomes	A	K	2457	31734
Masked genomes	A	L	1747	31734
Masked genomes	A	M	2483	31734
Masked genomes	A	N	976	31734
Masked genomes	A	O	836	31734
Masked genomes	A	P	2102	31734
Masked genomes	A	Q	1550	31734
Masked genomes	A	S	5574	31734
Masked genomes	A	T	2082	31734
Masked genomes	A	U	2768	31734
Masked genomes	A	V	479	31734
Masked genomes	A	W	2	31734
Masked genomes	B	A	3	14061
Masked genomes	B	B	7	14061
Masked genomes	B	C	983	14061
Masked genomes	B	D	120	14061
Masked genomes	B	E	1208	14061
Masked genomes	B	F	226	14061
Masked genomes	B	G	637	14061
Masked genomes	B	H	438	14061
Masked genomes	B	I	548	14061
Masked genomes	B	J	411	14061
Masked genomes	B	K	1232	14061
Masked genomes	B	L	864	14061
Masked genomes	B	M	858	14061
Masked genomes	B	N	391	14061
Masked genomes	B	O	368	14061
Masked genomes	B	P	865	14061
Masked genomes	B	Q	519	14061
Masked genomes	B	S	2761	14061
Masked genomes	B	T	940	14061
Masked genomes	B	U	917	14061
Masked genomes	B	V	183	14061
Masked genomes	B	W	2	14061
Masked genomes	C	A	2	6709
Masked genomes	C	B	4	6709
Masked genomes	C	C	421	6709
Masked genomes	C	D	62	6709
Masked genomes	C	E	640	6709
Masked genomes	C	F	142	6709
Masked genomes	C	G	304	6709
Masked genomes	C	H	229	6709
Masked genomes	C	I	298	6709
Masked genomes	C	J	258	6709
Masked genomes	C	K	618	6709
Masked genomes	C	L	276	6709
Masked genomes	C	M	403	6709
Masked genomes	C	N	190	6709
Masked genomes	C	O	204	6709
Masked genomes	C	P	450	6709
Masked genomes	C	Q	259	6709
Masked genomes	C	S	1397	6709
Masked genomes	C	T	418	6709
Masked genomes	C	U	290	6709
Masked genomes	C	V	88	6709
Masked genomes	C	W	1	6709
RGPs	A	A	5	31975
RGPs	A	B	16	31975
RGPs	A	C	1538	31975
RGPs	A	D	334	31975
RGPs	A	E	2325	31975
RGPs	A	F	469	31975
RGPs	A	G	1098	31975
RGPs	A	H	878	31975
RGPs	A	I	1217	31975
RGPs	A	J	659	31975
RGPs	A	K	2883	31975
RGPs	A	L	3286	31975
RGPs	A	M	1606	31975
RGPs	A	N	640	31975
RGPs	A	O	804	31975
RGPs	A	P	1749	31975
RGPs	A	Q	1027	31975
RGPs	A	S	6476	31975
RGPs	A	T	1581	31975
RGPs	A	U	1408	31975
RGPs	A	V	666	31975
RGPs	A	W	2	31975
RGPs	A	Z	2	31975
RGPs	B	A	2	20598
RGPs	B	B	7	20598
RGPs	B	C	977	20598
RGPs	B	D	189	20598
RGPs	B	E	1409	20598
RGPs	B	F	308	20598
RGPs	B	G	682	20598
RGPs	B	H	536	20598
RGPs	B	I	753	20598
RGPs	B	J	424	20598
RGPs	B	K	1948	20598
RGPs	B	L	2084	20598
RGPs	B	M	978	20598
RGPs	B	N	358	20598
RGPs	B	O	555	20598
RGPs	B	P	1079	20598
RGPs	B	Q	640	20598
RGPs	B	S	4454	20598
RGPs	B	T	915	20598
RGPs	B	U	859	20598
RGPs	B	V	411	20598
RGPs	B	W	4	20598
RGPs	B	Z	3	20598
RGPs	C	B	3	7496
RGPs	C	C	418	7496
RGPs	C	D	76	7496
RGPs	C	E	617	7496
RGPs	C	F	135	7496
RGPs	C	G	276	7496
RGPs	C	H	222	7496
RGPs	C	I	325	7496
RGPs	C	J	216	7496
RGPs	C	K	715	7496
RGPs	C	L	449	7496
RGPs	C	M	394	7496
RGPs	C	N	223	7496
RGPs	C	O	188	7496
RGPs	C	P	435	7496
RGPs	C	Q	235	7496
RGPs	C	S	1735	7496
RGPs	C	T	391	7496
RGPs	C	U	308	7496
RGPs	C	V	112	7496
RGPs	C	W	2	7496
